#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Adafruit_NeoPixel.h>
#include <ArduinoJson.h>

#define PIN 2
#define NUMPIXELS 1
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_RGB + NEO_KHZ800);

// Update these with values suitable for your network.
const char* ssid = "SSID";
const char* password = "WiFiPassword";
const char* mqtt_server = "mqttServer";
int timeLength;

WiFiClient espClient;
PubSubClient client(espClient);

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  StaticJsonBuffer<300> jsonBuffer;
  char json[length];
  for (int i = 0; i < length; i++) {
    json[i] = ((char)payload[i]);
    Serial.print((char)payload[i]);
  }
  Serial.println(" ");
  JsonObject& root = jsonBuffer.parseObject(json);
  if((String(topic)).indexOf("owntracks/user/device/event")==0){
    String event = root["event"].asString();
    String desc = root["desc"].asString();
    if((desc=="Ten Miles") && (event=="leave")) {
       Serial.print("Further than ten miles");
       pixels.setPixelColor(0, pixels.Color(255,0,0));
    }
    if(((desc=="Five Miles") && (event=="leave")) || ((desc=="Ten Miles") && (event=="enter"))){
       Serial.print("Ten Miles");
       pixels.setPixelColor(0, pixels.Color(239,15,0));
    } 
    if(((desc=="Two Miles") && (event=="leave")) || ((desc=="Five Miles") && (event=="enter"))){
       Serial.print("Five Miles");
       pixels.setPixelColor(0, pixels.Color(255,92,0));
    }
    if(((desc=="One Mile") && (event=="leave")) || ((desc=="Two Miles") && (event=="enter"))){
       Serial.print("Two Miles");
       pixels.setPixelColor(0, pixels.Color(255,158,11));
    }
    if(((desc=="Half Mile") && (event=="leave")) || ((desc=="One Mile") && (event=="enter"))){
       Serial.print("One Mile");
       pixels.setPixelColor(0, pixels.Color(214,187,6));
    }
    if(((desc=="Home") && (event=="leave")) || ((desc=="Half Mile") && (event=="enter"))){
       Serial.print("Half Mile");
       pixels.setPixelColor(0, pixels.Color(141,170,10));
    }
    if((desc=="Home") && (event=="enter")){
       Serial.print("Home");
       pixels.setPixelColor(0, pixels.Color(0,255,0));
    }
    pixels.show();
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("arduinoClient", "user", "pass")) {
      Serial.println("connected");
      client.subscribe("owntracks/user/device/event");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  Serial.begin(115200);
  pixels.begin();
  pixels.setPixelColor(0, pixels.Color(0,0,100));
  pixels.show();
  setup_wifi();
  client.setServer(mqtt_server, 16695);
  client.setCallback(callback);
  pixels.setPixelColor(0, pixels.Color(0,100,100));
  pixels.show();
  do{
    reconnect();
  }while(!client.connected());
  pixels.setPixelColor(0, pixels.Color(100,100,100));
  pixels.show();
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
}

