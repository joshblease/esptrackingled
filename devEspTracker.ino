//This development/custom version includes the use of a custom colour topic

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Adafruit_NeoPixel.h>
#include <ArduinoJson.h>

#define DEFAULTTIMELENGTH 1
#define PIN 2
#define NUMPIXELS 1
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_RGB + NEO_KHZ800);

// Update these with values suitable for your network.
const char* ssid = "SSID";
const char* password = "WiFiPassword";
const char* mqtt_server = "mqttServer";

//Values for custom colours
uint32_t locationColour;
boolean customColour = false;
unsigned long customColourStart;
int ledBrightness = 255;
int ledDirection = -1;
int red;
int green;
int blue;
String ledType="solid";
boolean customForever;
int timeLength;

WiFiClient espClient;
PubSubClient client(espClient);

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  StaticJsonBuffer<300> jsonBuffer;
  char json[length];
  for (int i = 0; i < length; i++) {
    json[i] = ((char)payload[i]);
    Serial.print((char)payload[i]);
  }
  Serial.println(" ");
  JsonObject& root = jsonBuffer.parseObject(json);
  if((String(topic)).indexOf("owntracks/user/device/event")==0){
    customColour = false;
    String event = root["event"].asString();
    String desc = root["desc"].asString();
    if((desc=="Ten Miles") && (event=="leave")) {
       Serial.print("Further than ten miles");
       locationColour = pixels.Color(255,0,0);
    }
    if(((desc=="Five Miles") && (event=="leave")) || ((desc=="Ten Miles") && (event=="enter"))){
       Serial.print("Ten Miles");
       locationColour = pixels.Color(239,15,0);
    } 
    if(((desc=="Two Miles") && (event=="leave")) || ((desc=="Five Miles") && (event=="enter"))){
       Serial.print("Five Miles");
       locationColour = pixels.Color(255,92,0);
    }
    if(((desc=="One Mile") && (event=="leave")) || ((desc=="Two Miles") && (event=="enter"))){
       Serial.print("Two Miles");
       locationColour = pixels.Color(255,158,11);
    }
    if(((desc=="Half Mile") && (event=="leave")) || ((desc=="One Mile") && (event=="enter"))){
       Serial.print("One Mile");
       locationColour = pixels.Color(214,187,6);
    }
    if(((desc=="Home") && (event=="leave")) || ((desc=="Half Mile") && (event=="enter"))){
       Serial.print("Half Mile");
       locationColour = pixels.Color(141,170,10);
    }
    if((desc=="Home") && (event=="enter")){
       Serial.print("Home");
       locationColour = pixels.Color(0,255,0);
    }
    pixels.setPixelColor(0, locationColour);
    pixels.show();
  }else if((String(topic)).indexOf("custom/colours")==0){
    red = root["red"].as<int>();
    green = root["green"].as<int>();
    blue = root["blue"].as<int>();
    ledType = root["type"].asString();
    customForever = root["forever"].as<boolean>();
    timeLength = root["time"].as<int>()*1000;
    if(timeLength == 0){
      timeLength = DEFAULTTIMELENGTH*1000;
    }
    customColour = true;
    customColourStart = millis();
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("arduinoClient", "user", "pass")) {
      Serial.println("connected");
      client.subscribe("owntracks/user/device/event");
      client.subscribe("custom/colours");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  Serial.begin(115200);
  pixels.begin();
  pixels.setPixelColor(0, pixels.Color(0,0,100));
  pixels.show();
  setup_wifi();
  client.setServer(mqtt_server, 16695);
  client.setCallback(callback);
  pixels.setPixelColor(0, pixels.Color(0,100,100));
  pixels.show();
  do{
    reconnect();
  }while(!client.connected());
  pixels.setPixelColor(0, pixels.Color(100,100,100));
  pixels.show();
  locationColour = pixels.Color(100,100,100);
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  if(customColour){
    if(ledType=="flash"){
      pixels.setBrightness(255);
      pixels.setPixelColor(0, pixels.Color(red,green,blue));
      pixels.show();
      delay(400);
      pixels.setPixelColor(0, pixels.Color(0,0,0));
      pixels.show();
      delay(400);
    }else if(ledType=="fade"){
      pixels.setPixelColor(0, pixels.Color(red,green,blue));
      pixels.setBrightness(ledBrightness);
      pixels.show();
      ledBrightness += (ledDirection * 5);
      if(ledBrightness < 30 || ledBrightness >= 255){
        ledDirection *= -1;
      }
      delay(25);
    }else{
      pixels.setPixelColor(0, pixels.Color(red,green,blue));
      pixels.show();
    }
  }
  if (millis()-customColourStart>timeLength && !customForever) {
    customColour = false;
    ledBrightness = 255;
    ledDirection = -1;
    pixels.setBrightness(255);
    pixels.setPixelColor(0, locationColour);
    pixels.show();
  }
}

